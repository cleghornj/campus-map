/*
	Our data array for possible location names and their lngLat.
	This array should be a 1-to-1 with the drop down selection box on the HTML page
	at least in terms of HMTL 'value' matching array position.
*/
var building_data = [
	{
	"name":"McCord",
	"lon":"-87.3547",
	"lat":"36.53191",
	"info":"this is a building McCord"
	},
	{
	"name":"Harned Hall",
	"lon":"-87.35295",
	"lat":"36.53412",
	"info":"this is a building Harned"
	},
	{
	"name":"Foy Recreation Center",
	"lon":"-87.35403",
	"lat":"36.53569"
	},
	{
	"name":"Winfield Dunn Center",
	"lon":"-87.35666",
	"lat":"36.53624"
	},
	{
	"name":"Marks",
	"lon":"-87.35326",
	"lat":"36.5345"
	},
	{
	"name":"Morgan University Center",
	"lon":"-87.35395",
	"lat":"36.53309"
	},
	{
	"name":"Sundquist Science Complex",
	"lon":"-87.35077",
	"lat":"36.53266"
	},
	{
	"name":"Claxton",
	"lon":"-87.35246",
	"lat":"36.53207"
	},
	{
	"name":"Clement",
	"lon":"-87.35339",
	"lat":"36.53219"
	},
	{
	"name":"Woodward Library",
	"lon":"-87.35267",
	"lat":"36.53271"
	},
	{
	"name":"Castle Heights Dormitory",
	"lon":"-87.35786",
	"lat":"36.53379"
	},
	{
	"name":"Trahern",
	"lon":"-87.35094",
	"lat":"36.53353"
	},
	{
	"name":"Hemlock Semiconductor",
	"lon":"-87.34963",
	"lat":"36.53273"
	},
	{
	"name":"Shasteen",
	"lon":"-87.35249",
	"lat":"36.53792"
	},
	{
	"name":"Kimbrough Hall",
	"lon":"-87.35149",
	"lat":"36.53484"
	},
	{
	"name":"Music/Mass Communication",
	"lon":"-87.35086",
	"lat":"36.53461"
	},
	{
	"name":"Browning",
	"lon":"-87.35415",
	"lat":"36.53231"
	},
	{
	"name":"Ellington",
	"lon":"-87.35541",
	"lat":"36.53263"
	},
	{
	"name":"The Think Tank",
	"lon":"-87.3518",
	"lat":"36.53257"
	},
	{
	"name":"Harvill Bookstore",
	"lon":"-87.35382",
	"lat":"36.53406"
	},
	{
	"name":"Power Plant",
	"lon":"-87.35444",
	"lat":"36.53308"
	}
		
];


/*
	Create global variables for our current marker and the marker group.
	These allow us to wipe the old one and then create a new one.
*/
var current_marker, marker_group;

/*
	We need to clear the layer group every time, but the FIRST time through,
	the global marker & group have no value.  To get around this, we create
	a boolean to keep track if this is the first time the ping function is ran.
	If so, it will set the boolean to "false" at the end of the ping function.

*/
var first_selection = true;


function ping(selectID){
	
	//Grab the value of the building in the list box and match it with the building in the data array
	var bldgSelect = document.getElementById("selectID");
	var bldg = document.getElementById(selectID).value;
	
	//Grab latitude & longitude values
	var lat = building_data[bldg].lat;
	var lon = building_data[bldg].lon;
	//Grab building info
	//var bldgInfo = building_data[bldg].info;
	//Clean-up the old marker if it's not the first time through
	if(!first_selection){
		map.removeLayer(marker_group);
	}
	
	//Define our marker, add it to the group, add it to the map, and open the pop-up
	current_marker = L.marker([lat, lon]).bindPopup(building_data[bldg].name);
	marker_group = L.layerGroup([current_marker]);
	marker_group.addTo(map);
	current_marker.openPopup()
	
	//Move to the location
	map.panTo(new L.LatLng(lat, lon));
	
	//Add building info to legend
	//addedInfo.innerHTML = bldgInfo;
	//Set the 'first time' boolean to false so we can clean-up markers
	first_selection = false;
}


